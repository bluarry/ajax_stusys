
 /*字符串格式化*/
String.prototype.format = function(args) {
	var result = this;
	if (arguments.length < 1) {
		return result;
	}

	var data = arguments; // 如果模板参数是数组
	if (arguments.length == 1 && typeof (args) == "object") {
		// 如果模板参数是对象
		data = args;
	}
	for ( var key in data) {
		var value = data[key];
		if (undefined != value) {
			result = result.replace("{" + key + "}", value);
		}
	}
	return result;
}

function getxmlhttp(){
	var xmlHttp;
	if (window.ActiveXObject) {// 创建xmlHttpRequest对象
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	} else if (window.XMLHttpRequest) {
		xmlHttp = new XMLHttpRequest();
	}
	
	return xmlHttp;
}

function showstuinfo(strurl){
	var xmlHttp;
	xmlHttp=getxmlhttp();

	var url=strurl;
	
	xmlHttp.open("GET",strurl,true);
	
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4){
			if(xmlHttp.status==200){
				var xmlret=xmlHttp.responseXML;
				var students=xmlret.getElementsByTagName("student");
				for(var i=0;i<students.length;i++){
					var Sno=	students[i].childNodes[0].firstChild.data;
					var Sname =	students[i].childNodes[1].firstChild.data;
					var Sex=	students[i].childNodes[2].firstChild.data;
					var Sage=	students[i].childNodes[3].firstChild.data;
					var Sethnic=	students[i].childNodes[4].firstChild.data;
					var Steacher=	students[i].childNodes[5].firstChild.data;
					var Sclass=	students[i].childNodes[6].firstChild.data;
					var Sdept=	students[i].childNodes[7].firstChild.data;
					var items="<tr class='success'>"
						+ " <td>{0}</td>".format(Sno)
						+ " <td>{0}</td>".format(Sname)
						+ " <td>{0}</td>".format(Sex)
						+ " <td>{0}</td>".format(Sage)
						+ " <td>{0}</td>".format(Sethnic)
						+ " <td>{0}</td>".format(Steacher)
						+ " <td>{0}</td>".format(Sclass)
						+ " <td>{0}</td>".format(Sdept)
						+ " </tr>";
					var tbody=document.getElementById("stuinfo_table_body");
					var x=tbody.insertRow(tbody.size);
					x.innerHTML=items;
				}
			}
		}
	}
	xmlHttp.send(null);
}
function clearstuinfo(){
	var tbody=document.getElementById("stuinfo_table_body");
	tbody.innerHTML="";
}