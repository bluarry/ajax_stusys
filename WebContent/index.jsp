<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/all.js"></script>
    <title>简单学籍管理系统</title>
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">简单学籍管理系统</a>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-2">
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        		操作菜单
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href='javascript:showstuinfo("<c:url value='/getUserinfoServlet'/>");'>显示学生学籍信息</a></li>
                        <li><a href="javascript:clearstuinfo()">清除学籍列表信息</a></li>
                        <!-- 
                        <li role="separator" class="divider"></li>
                        <li><a href="#"></a></li>
                         -->
                    </ul>
                </div>
        </div>
        <div class="col-md-10">
            <table id="stuinfo" class="table table-bordered ">
                <thead id="stuinfo_table_header">
                <tr>
                <!--
                	select 
                	table
    				学籍信息包括:
     				 学号 姓名 性别 年龄 民族 导师 班级 专业
				-->
                    <th>学号</th>
                    <th>姓名</th>
                    <th>性别</th>
                    <th>年龄</th>
                    <th>民族</th>
                    <th>导师</th>
                    <th>班级</th>
                    <th>专业</th>
                </tr>
                </thead>
                <tbody id="stuinfo_table_body">
                </tbody>
            </table>


        </div>

    </div>
</div>


</body>
</html>