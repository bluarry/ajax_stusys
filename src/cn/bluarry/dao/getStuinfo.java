package cn.bluarry.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import cn.bluarry.table.Student;

public class getStuinfo {
	
	
	public static List<Student> getstudents() throws SQLException {
		List<Student> list=new ArrayList<Student>();
		
		Connection conn=GetConnection.Getconn();
		
		String sql="SELECT * FROM Stu;";
		
		Statement stats=conn.createStatement();
		
		ResultSet rs=stats.executeQuery(sql);
		
	    /*
			学籍信息包括:
			 学号 姓名 性别 年龄 民族 导师 班级 专业
		*/
		while(rs.next()){
			Student s=new Student();
			s.setSno(rs.getString("Sno"));
			s.setSname(rs.getString("Sname"));
			s.setSex(rs.getString("Sex"));
			s.setSage(rs.getInt("Sage"));
			s.setSethnic(rs.getString("Sethnic"));
			s.setSteacher(rs.getString("Steacher"));
			s.setSdept(rs.getString("Sdept"));
			s.setSclass(rs.getString("Sclass"));
			list.add(s);
		}		
		GetConnection.CloseConn();
		return list;
	}
	
//	public static void main(String[] args) {
//		try {
//			getstudents();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//	
	
}
