package cn.bluarry.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class GetConnection {
	private static Connection conn=null;
	
	private GetConnection(){};
	
	public static Connection Getconn(){
		if(conn==null){
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

				String URL = "jdbc:sqlserver://localhost:1433;DatabaseName=Stuinfo";

				conn = DriverManager.getConnection(URL, "sa", "hehe"); 

			} catch (Exception e) {
				conn=null;
			}
			
		}
		return conn;
	}
	public static boolean CloseConn(){
		boolean suc=false;
		try {
			conn.close();
		} catch (Exception e) {
			suc=false;
		}
		return suc;
	}

}
