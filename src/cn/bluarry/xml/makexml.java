package cn.bluarry.xml;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import cn.bluarry.dao.getStuinfo;
import cn.bluarry.table.Student;

public class makexml {
	public static Document getstuxml() throws SQLException{
		Document document = DocumentHelper.createDocument();
		
		List<Student> list=getStuinfo.getstudents();
		Element stuinfo = document.addElement("stuinfo");
		
	   /*
			学籍信息包括:
			 学号 姓名 性别 年龄 民族 导师 班级 专业
		*/
		
		for(Student x:list){
			Element stu=stuinfo.addElement("student");
			stu.addElement("Sno").addText(x.getSno());
			stu.addElement("Sname").addText(x.getSname());
			stu.addElement("Sex").addText(x.getSex());
			stu.addElement("Sage").addText(new Integer(x.getSage()).toString());
			stu.addElement("Sethnic").addText(x.getSethnic());
			stu.addElement("Steacher").addText(x.getSteacher());
			stu.addElement("Sclass").addText(x.getSclass());
			stu.addElement("Sdept").addText(x.getSdept());
		}
		
		return document;
	}
	
//	public static void main(String[] args) throws IOException, SQLException {
//		Document document = DocumentHelper.createDocument();
//		
//		
//		
//		
//		Element root = document.addElement("root");
//		Element author1 = root.addElement("author").addAttribute("name", "James").addAttribute("location", "UK")
//				.addText("James Strachan");
//		Element author2 = root.addElement("author").addAttribute("name", "Bob").addAttribute("location", "US")
//				.addText("Bob McWhirter");
//		
	
//		Document document=getstuxml();
//		
//		XMLWriter writer=new XMLWriter(new FileWriter("foo.xml"));
//		
//		writer.write(document);
//		writer.close();
//		
//		System.out.println("suc");
//	}
	
	
	
	
	
	
	
}
