package cn.bluarry.table;

public class Student {

	private String Sno;
	private String Sname;
	private String Sex;
	private int Sage;
	private String Sethnic;
	private String Steacher;
	private String Sclass;
	private String Sdept;
	public String getSno() {
		return Sno;
	}
	public void setSno(String sno) {
		Sno = sno;
	}
	public String getSname() {
		return Sname;
	}
	public void setSname(String sname) {
		Sname = sname;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public int getSage() {
		return Sage;
	}
	public void setSage(int sage) {
		Sage = sage;
	}
	public String getSethnic() {
		return Sethnic;
	}
	public void setSethnic(String sethnic) {
		Sethnic = sethnic;
	}
	public String getSteacher() {
		return Steacher;
	}
	public void setSteacher(String steacher) {
		Steacher = steacher;
	}
	public String getSclass() {
		return Sclass;
	}
	public void setSclass(String sclass) {
		Sclass = sclass;
	}
	public String getSdept() {
		return Sdept;
	}
	public void setSdept(String sdept) {
		Sdept = sdept;
	}
	
	@Override
	public String toString() {
		String str="{ Sno:"+Sno+","
				+ " Sname: "+Sname+","
				+ " Sex: "+Sex+","
				+ " Sage: "+Sage+","
				+ " Sethnic: "+Sethnic+","
				+ " Steacher: "+Steacher+","
				+ " Sclass: "+Sclass+","
				+ " Sdept: "+Sdept
				+ " }";
		return str;
	}
	
}
